package ogb;

import robocode.Robot;
import robocode.ScannedRobotEvent;

/**
 * A demonstration of the onScannedRobot method.
 * 
 * @author Branden Ogata
 *
 */
public class ScanEventDemo extends Robot
{
  /**
   * Rotates the radar back and forth.
   */
  @Override
  public void run()
  {
    while (true)
    {
      turnRadarLeft(15);
      turnRadarRight(30);
    }
  }
  
  /**
   * Upon detecting another robot, moves forward and backward.
   * 
   * @param event    The ScannedRobotEvent that recorded the scan.
   * 
   */
  @Override
  public void onScannedRobot(ScannedRobotEvent event)
  {
    ahead(32);
    back(32);
  }
}

