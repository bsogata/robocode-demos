package ogb;

import robocode.Robot;

/**
 * A demonstration of the getGunHeat and getGunCoolingRate methods.
 * 
 * @author Branden Ogata
 *
 */
public class CannonHeatDemo extends Robot
{
  /**
   * Turns the gun and fires blindly.
   */
  @Override
  public void run()
  {
    int angle = 1;
    
    while (true) 
    {
      turnGunRight(angle++);
      
      while (getGunHeat() > 0) 
      {
        scan();
      }
      
      fire(angle % 4);
      turnGunLeft(angle++);

      for (double i = getGunHeat(); i > 0; i -= getGunCoolingRate()) 
      {
        scan();
      }
      
      fire(angle % 4);
    }
  }
}


