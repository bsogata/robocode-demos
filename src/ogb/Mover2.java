package ogb;

import java.awt.Color;
import ogb.util.Utility;
import robocode.Robot;

/**
 * Moves to the center of the field.
 * 
 * @author Branden Ogata
 *
 */
public class Mover2 extends Robot
{
  /**
   * Moves to the center of the field.
   */
  @Override
  public void run()
  {
    // The most important line
    this.setColors(Color.WHITE, new Color(217, 217, 217), new Color(0, 32, 96));

    // Move to the center
    Utility.move(this, this.getBattleFieldWidth() / 2, this.getBattleFieldHeight() / 2);
  }
}
