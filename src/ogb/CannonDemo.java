package ogb;

import robocode.Robot;

/**
 * A demonstration of the turnGunLeft, turnGunRight, and fire methods.
 * 
 * @author Branden Ogata
 *
 */
public class CannonDemo extends Robot
{
  /**
   * Turns the gun and fires blindly.
   */
  @Override
  public void run()
  {
    int angle = 1;
    
    while (true) 
    {
      turnGunRight(angle++);
      fire(angle % 4);
      turnGunLeft(angle++);
      fire(angle % 4);
    }
  }
}


