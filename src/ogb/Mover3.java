package ogb;

import java.awt.Color;
import ogb.util.Utility;
import robocode.Robot;

/**
 * Moves to the four corners of the field.
 * 
 * @author Branden Ogata
 *
 */
public class Mover3 extends Robot
{
  /**
   * Moves to each corner on the field.
   */
  @Override
  public void run()
  {
    // The most important line
    this.setColors(Color.WHITE, new Color(217, 217, 217), new Color(0, 32, 96));

    // Lower left
    Utility.move(this, 32, 32);
    
    // Upper left
    Utility.move(this, 32, this.getBattleFieldHeight() - 32);
    
    // Upper right
    Utility.move(this, this.getBattleFieldWidth() - 32, this.getBattleFieldHeight() - 32);
    
    // Lower right
    Utility.move(this, this.getBattleFieldWidth() - 32, 32);
  }
}
