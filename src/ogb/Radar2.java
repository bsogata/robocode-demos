package ogb;

import java.awt.Color;
import robocode.Robot;
import robocode.ScannedRobotEvent;

/**
 * Attempts to track another robot with radar.
 * 
 * @author Branden Ogata
 *
 */
public class Radar2 extends Robot
{
  /**
   * The direction to rotate the radar; 1 for clockwise, -1 for counterclockwise.
   */
  private int direction = 1;
  
  /**
   * Rotates the radar.
   */
  @Override
  public void run()
  {
    // The most important line
    this.setColors(Color.WHITE, new Color(217, 217, 217), new Color(0, 32, 96));

    while (true)
    {
      turnRadarRight(360 * this.direction);
    }
  }
  
  /**
   * Changes direction of the radar spin.
   * 
   * @param e    The ScannedRobotEvent that recorded the scan.
   * 
   */
  @Override
  public void onScannedRobot(ScannedRobotEvent e)
  {
    this.direction *= -1;
    turnRadarRight(this.direction * ((this.getHeading() + e.getBearing()) - this.getRadarHeading()));
  }
}
