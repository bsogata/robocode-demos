package ogb;

import robocode.Robot;

/**
 * A demonstration of the turnRadarLeft and turnRadarRight methods.
 * 
 * @author Branden Ogata
 *
 */
public class RadarDemo extends Robot
{
  /**
   * Turns the radar left and right.
   */
  @Override
  public void run()
  {
    turnRadarLeft(360);
    turnRadarLeft(-360);
    turnRadarRight(360);
    turnRadarRight(-360);
  }
}


