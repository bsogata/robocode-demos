package ogb;

import java.awt.Color;
import robocode.Robot;

/**
 * Rotates the radar.
 * 
 * @author Branden Ogata
 *
 */
public class Radar0 extends Robot
{
  /**
   * Rotates the radar.
   */
  @Override
  public void run()
  {
    // The most important line
    this.setColors(Color.WHITE, new Color(217, 217, 217), new Color(0, 32, 96));

    while (true)
    {
      turnRadarRight(360);
    }
  }
}
