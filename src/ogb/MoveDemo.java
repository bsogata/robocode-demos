package ogb;

import robocode.Robot;

/**
 * A demonstration of the ahead and back methods in Robocode.
 * 
 * @author Branden Ogata
 *
 */
public class MoveDemo extends Robot
{
  /**
   * Moves the robot forward and backward.
   */
  @Override
  public void run()
  {
    ahead(64);
    ahead(-64);
    back(64);
    back(-64);
  }
}
