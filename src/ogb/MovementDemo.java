package ogb;

import robocode.Robot;

/**
 * A simple demonstration of how movement works in Robocode.
 * 
 * @author Branden Ogata
 *
 */
public class MovementDemo extends Robot
{
  /**
   * Moves the robot.
   */
  @Override
  public void run()
  {
    ahead(100);
    back(100);
    turnLeft(90);
    turnRight(90);
  }
}
