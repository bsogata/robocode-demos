package ogb;

import robocode.Robot;

/**
 * Demonstrates dependent and independent cannon and radar movement.
 * 
 * @author Branden Ogata
 *
 */
public class CannonAdjustDemo extends Robot
{
  /**
   * Rotates with and without cannon and radar adjustment.
   */
  @Override
  public void run()
  {
    while (true)
    {
      this.setAdjustGunForRobotTurn(false);
      this.turnLeft(360); 
      this.setAdjustGunForRobotTurn(true);
      this.turnRight(180);
      this.setAdjustRadarForGunTurn(false);
      this.turnGunLeft(360); 
      this.setAdjustRadarForGunTurn(true);
      this.turnGunRight(180);
    }
  }
}

