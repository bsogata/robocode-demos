package ogb;

import java.awt.Color;
import robocode.Robot;

/**
 * Moves forward 100 pixels per turn.
 * 
 * @author Branden Ogata
 *
 */
public class Mover0 extends Robot
{
  /**
   * Moves forward 100 pixels per turn.
   */
  @Override
  public void run()
  {
    // The most important line
    this.setColors(Color.WHITE, new Color(217, 217, 217), new Color(0, 32, 96));

    while (true)
    {
      ahead(100);
    }
  }
}
