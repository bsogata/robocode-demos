package ogb;

import robocode.Robot;

/**
 * Demonstrates dependent and independent radar movement.
 * 
 * @author Branden Ogata
 *
 */
public class RadarAdjustDemo extends Robot
{
  /**
   * Rotates with and without radar adjustment.
   */
  @Override
  public void run()
  {
    while (true)
    {
      this.setAdjustRadarForRobotTurn(false);
      this.turnLeft(360); 
      this.setAdjustRadarForRobotTurn(true);
      this.turnRight(180);
    }
  }
}

