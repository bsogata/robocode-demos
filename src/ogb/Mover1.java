package ogb;

import java.awt.Color;
import robocode.Robot;

/**
 * Moves forward n pixels then turn to the right, increasing n by 16 degrees on each iteration.
 * 
 * @author Branden Ogata
 *
 */
public class Mover1 extends Robot
{
  /**
   * Moves forward n pixels then turn to the right, increasing n by 15 degrees on each iteration.
   */
  @Override
  public void run()
  {
    // The most important line
    this.setColors(Color.WHITE, new Color(217, 217, 217), new Color(0, 32, 96));

    int toMove = 16;
    
    while (true)
    {
      ahead(toMove);
      toMove += 16;
      turnRight(90);
    }
  }
}
