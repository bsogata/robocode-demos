package ogb;

import robocode.Robot;

/**
 * A simple Hello World robot.
 * 
 * @author Branden Ogata
 *
 */
public class HelloWorld extends Robot
{
  /**
   * Prints "Hello World" to the console.
   */
  @Override
  public void run()
  {
    System.out.println("Hello World");
  }
}

