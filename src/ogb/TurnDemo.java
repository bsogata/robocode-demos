package ogb;

import robocode.Robot;

/**
 * A demonstration of the turnLeft and turnRight methods in Robocode.
 * 
 * @author Branden Ogata
 *
 */
public class TurnDemo extends Robot
{
  /**
   * Turns the robot left and right.
   */
  @Override
  public void run()
  {
    turnLeft(360);
    turnLeft(-360);
    turnRight(360);
    turnRight(-360);
  }
}
