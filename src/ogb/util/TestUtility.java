package ogb.util;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Tests the Utility class.
 * 
 * @author Branden Ogata
 *
 */
public class TestUtility
{
  /**
   * Tests the distance method.
   */
  @Test
  public void testDistance()
  {
    assertEquals("Same point should have distance of zero", 
                 0.0, Utility.distance(0, 0, 0, 0), Utility.THRESHOLD);
    assertEquals("Same point should have distance of zero", 
                 0.0, Utility.distance(6.4, 3.2, 6.4, 3.2), Utility.THRESHOLD);
    
    assertEquals("(0, 0), (0, 1) should have distance of one",
                 1.0, Utility.distance(0, 0, 0, 1), Utility.THRESHOLD);
    assertEquals("(0, 0), (1, 0) should have distance of one",
                 1.0, Utility.distance(0, 0, 1, 0), Utility.THRESHOLD);
    assertEquals("(0, 0), (0, -1) should have distance of one",
                 1.0, Utility.distance(0, 0, 0, -1), Utility.THRESHOLD);
    assertEquals("(0, 0), (-1, 0) should have distance of one",
                 1.0, Utility.distance(0, 0, -1, 0), Utility.THRESHOLD);
    
    assertEquals("(1.6, 1.6), (2.6, 2.6) should have distance sqrt(2)",
                 Math.sqrt(2), Utility.distance(1.6, 1.6, 2.6, 2.6), Utility.THRESHOLD);
    assertEquals("(1.6, 1.6), (0.6, 0.6) should have distance sqrt(2)",
                 Math.sqrt(2), Utility.distance(1.6, 1.6, 0.6, 0.6), Utility.THRESHOLD);
  }
  
  /**
   * Tests the heading method.
   */
  @Test
  public void testHeading()
  {
    assertEquals("Same point should have heading of zero", 
                 0.0, Utility.heading(0, 0, 0, 0), Utility.THRESHOLD);
    assertEquals("Same point should have heading of zero", 
                 0.0, Utility.heading(6.4, 3.2, 6.4, 3.2), Utility.THRESHOLD);
    
    assertEquals("(0, 0), (0, 1) should have heading of 0",
                 0.0, Utility.heading(0, 0, 0, 1), Utility.THRESHOLD);
    assertEquals("(0, 0), (1, 0) should have heading of PI/2",
                 Math.PI / 2, Utility.heading(0, 0, 1, 0), Utility.THRESHOLD);
    assertEquals("(0, 0), (0, -1) should have heading of PI",
                 Math.PI, Utility.heading(0, 0, 0, -1), Utility.THRESHOLD);
    assertEquals("(0, 0), (-1, 0) should have heading of 3PI/2",
                 -Math.PI / 2, Utility.heading(0, 0, -1, 0), Utility.THRESHOLD);
    
    assertEquals("(1.6, 1.6), (2.6, 2.6) should have heading of PI/4",
                 Math.PI / 4, Utility.heading(1.6, 1.6, 2.6, 2.6), Utility.THRESHOLD);
    assertEquals("(1.6, 1.6), (0.6, 0.6) should have heading of 5PI/4",
                 -(3 * Math.PI) / 4, Utility.heading(1.6, 1.6, 0.6, 0.6), Utility.THRESHOLD);
  }

}
