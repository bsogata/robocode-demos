package ogb.util;

import robocode.Robot;

/**
 * Helper methods for Robocode robots.
 * 
 * @author Branden Ogata
 *
 */
public class Utility
{
  /**
   * The arbitrary threshold for what constitutes an acceptable deviation from equality.
   */
  public static final double THRESHOLD = 0.0001;
  
  /**
   * Returns the distance between two points.
   * 
   * @param x0    The double equal to the x-coordinate of the first point.
   * @param y0    The double equal to the y-coordinate of the first point.
   * @param x1    The double equal to the x-coordinate of the second point.
   * @param y1    The double equal to the y-coordinate of the second point.
   * 
   * @return A double equal to the distance between the two points.
   * 
   */
  public static double distance(double x0, double y0, double x1, double y1)
  {
    return Math.sqrt(Math.pow((x1 - x0), 2) + Math.pow((y1 - y0), 2));
  }
  
  /**
   * Returns the absolute heading from the first point to the second point.
   * 
   * @param x0    The double equal to the x-coordinate of the first point.
   * @param y0    The double equal to the y-coordinate of the first point.
   * @param x1    The double equal to the x-coordinate of the second point.
   * @param y1    The double equal to the y-coordinate of the second point.
   * 
   * @return A double equal to the heading from point 0 to point 1.
   * 
   */
  public static double heading(double x0, double y0, double x1, double y1)
  {
    return Math.atan2(x1 - x0, y1 - y0);
  }
  
  /**
   * Moves the given Robot to the indicated coordinates.
   *
   * @param robot    The Robot to move.
   * @param x        The double equal to the x-coordinate to move to.
   * @param y        The double equal to the y-coordinate to move to.
   * 
   */
  public static void move(Robot robot, double x, double y)
  {
    robot.turnRight(Math.toDegrees(Utility.heading(robot.getX(), robot.getY(), x, y)) -
                                   robot.getHeading());
                   
    // Move forward to reach the center
    robot.ahead(Utility.distance(robot.getX(), robot.getY(), x, y));
  }
}
